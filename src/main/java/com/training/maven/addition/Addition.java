package com.training.maven.addition;

public class Addition {
	private int a,b;
	
	public Addition(int nb1, int nb2) {
		this.a = nb1; 
		this.b = nb2;
							
	}

	public int somme () {
		return (a + b);
	}
	
	public static void main (String[] argd) {
		
		Addition myadd = new Addition(5, 6); 
	  
		System.out.println(myadd.somme());
	}
}
